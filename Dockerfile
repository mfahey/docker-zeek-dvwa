FROM debian:9.2

LABEL maintainer "https://gitlab.com/mfahey/docker-zeek-dvwa"

#### DVWA Dependecies
RUN apt-get update && \
    apt-get upgrade -y && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
    debconf-utils && \
    echo mariadb-server mysql-server/root_password password vulnerables | debconf-set-selections && \
    echo mariadb-server mysql-server/root_password_again password vulnerables | debconf-set-selections && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
    apache2 \
    mariadb-server \
    php \
    bash \
    php-mysql \
    php-pgsql \
    php-pear \
    php-gd

#### Zeek Dependencies 
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
    cmake \
    make \
    gcc \
    g++ \
    flex \
    bison \
    git \
    libpcap-dev \
    libssl-dev \
    python-dev \
    swig \
    zlib1g-dev \
    curl && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

### Make Zeek
RUN cd /tmp && \
    git clone --recursive https://github.com/zeek/zeek && \
    cd /tmp/zeek && \
    ./configure && \
    make && \
    make install

COPY php.ini /etc/php5/apache2/php.ini
COPY dvwa /var/www/html

COPY config.inc.php /var/www/html/config/

RUN chown www-data:www-data -R /var/www/html && \
    rm /var/www/html/index.html

RUN service mysql start && \
    sleep 3 && \
    mysql -uroot -pvulnerables -e "CREATE USER app@localhost IDENTIFIED BY 'vulnerables';CREATE DATABASE dvwa;GRANT ALL privileges ON dvwa.* TO 'app'@localhost;"

### RUN Zeek
CMD 

EXPOSE 80

VOLUME [ "/data" ]

COPY main.sh /
ENTRYPOINT ["/main.sh"]
